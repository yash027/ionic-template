import { Component, OnInit } from "@angular/core";
import { FingerprintAIO } from "@ionic-native/fingerprint-aio/ngx";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  constructor(private faio: FingerprintAIO) {}

  ngOnInit() {}

  authFingerprint() {
    this.faio.isAvailable().then(result => {
      if (result === "OK") {
        this.faio
          .show({
            title: "Scan Your Fingerprint",
            fallbackButtonTitle: "Use login creadentials",
            disableBackup: true,
            description:
              "scan the finger which you entered in your phone settings."
          })
          .then((result: any) => {
            console.log(result);
            alert(result);
          })
          .catch((error: any) => {
            console.log(error);
            alert(error);
          });
      }
    });
  }
}
