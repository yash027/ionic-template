import { Component, OnInit } from "@angular/core";
import { MenuController } from "@ionic/angular";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.page.html",
  styleUrls: ["./menu.page.scss"]
})
export class MenuPage implements OnInit {
  index = ["", "", "", ""];
  constructor(private menuctrl: MenuController) {}

  ngOnInit() {}

  toggleMenu() {
    this.menuctrl.toggle("content");
  }

  doRefresh(event) {
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}
